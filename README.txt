*第一版上傳測試

因時間使用過多還有很多技能不熟練的情況 (mongodb 第一次使用，圖文解碼第一次破解 [成功滿開心的])
就先交出目前進度給予評估


1. 請注意資料夾是否有下載chromedriver.exe
如果沒有請至 https://chromedriver.chromium.org/ 找到對應使用者自己的chrome 版本下載
才能正常啟用selenium

2. 啟動 interview.py (使用pycharm)
請設定好 176/ 177的mongodb 帳號密碼

3. 確認是否有新北市道路清單，如果沒有請使用181的 crawl_and_save_new_taipei_road_name 做抓取

4. 如果確認 有新北市道路清單 可以啟用183的save_all_license 來做抓取執照清單的動作
*目前卡在執照和區域必須有點擊過或是呼叫點擊後的function才可以執行搜尋