# Go over each country in https://www.globalspec.com/selectcountry/
import requests
import time
import random
import sys
import threading
import urllib3
from lxml import etree
import json
import traceback
import re
import mysql.connector
import datetime
from inspect import currentframe, getframeinfo
import urllib.request
import codecs
import bs4
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as expected
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import ssl
from stem import Signal
from stem.control import Controller
from fake_useragent import UserAgent
from stop_words import get_stop_words
import base64
import pytesseract
from PIL import Image
import cv2
from pymongo import MongoClient


def write_error(e, error_line=False):
    if error_line is not False:
        log_text = time.strftime("%m/%d/%Y, %H:%M:%S", time.localtime()) + " | line: " + str(error_line) + "\n"
        file = open('error_log.txt', "a", encoding="utf-8")
        file.write(str(log_text))
        file.close()

    error_class = e.__class__.__name__  # 取得錯誤類型
    detail = e.args[0]  # 取得詳細內容
    cl, exc, tb = sys.exc_info()  # 取得Call Stack
    lastCallStack = traceback.extract_tb(tb)[-1]  # 取得Call Stack的最後一筆資料
    fileName = lastCallStack[0]  # 取得發生的檔案名稱
    lineNum = lastCallStack[1]  # 取得發生的行號
    funcName = lastCallStack[2]  # 取得發生的函數名稱
    errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
    log_text = time.strftime("%m/%d/%Y, %H:%M:%S", time.localtime()) + " " + str(errMsg) + "\n"
    file = open('error_log.txt', "a", encoding="utf-8")
    file.write(str(log_text))
    file.close()


def db_insert_one(collection_name, data):
    collection_name.insert_one(data)
    # collection_name.insert_many(docs)


def crawl_and_save_new_taipei_road_name(client):

    ua = UserAgent()
    U_Agent = ua.random
    OPTIONS = Options()
    OPTIONS.add_argument('--no-sandbox')
    OPTIONS.add_argument('--disable-dev-shm-usage')
    OPTIONS.add_argument('--disable-gpu')
    OPTIONS.add_argument('--headless')
    extset = ['enable-automation', 'ignore-certificate-errors']
    OPTIONS.add_experimental_option("excludeSwitches", extset) #新版-關閉自動測試軟件
    OPTIONS.add_argument("user-agent="+U_Agent)

    url = "https://zip5.5432.tw/cityzip/%E6%96%B0%E5%8C%97%E5%B8%82"
    driver = webdriver.Chrome(options=OPTIONS)
    driver.get(url)

    soup = bs4.BeautifulSoup(driver.page_source, 'html.parser')
    dom = etree.HTML(str(soup))

    all_list = []
    if dom.xpath('//table[@id="zip-table2"]//td[contains(@class,"zip-area")]'):
        for area_td_dom in dom.xpath('//table[@id="zip-table2"]//tr'):
            area_td_dom_etree = etree.HTML(etree.tostring(area_td_dom))
            area_info_list = {}
            name = area_td_dom_etree.xpath('//td[contains(@class,"zip-area")]/a/text()')[0].strip()
            url = area_td_dom_etree.xpath('//td[contains(@class,"zip-area")]/a/@href')[0].strip()
            if url[:1] == "/":
                url = "https://zip5.5432.tw/" + url
            area_info_list[name] = url
            all_list.append(area_info_list)

    driver.close()
    # print(all_list)

    for data in all_list:
        for name, url in data.items():
            driver = webdriver.Chrome(options=OPTIONS)
            driver.get(url)
            print(url)

            soup = bs4.BeautifulSoup(driver.page_source, 'html.parser')
            dom = etree.HTML(str(soup))

            this_area_name_list = []
            if dom.xpath('//table[@id="zip-table"]//td[contains(@class,"zip-road")]'):
                for area_tr_dom in dom.xpath('//table[@id="zip-table"]//tr'):
                    area_tr_dom_etree = etree.HTML(etree.tostring(area_tr_dom))
                    for area_td_dom in area_tr_dom_etree.xpath('//td[contains(@class,"zip-road")]'):
                        area_td_dom_etree = etree.HTML(etree.tostring(area_td_dom))
                        area_td_name = area_td_dom_etree.xpath('//a/text()')[0].strip()
                        this_area_name_list.append(area_td_name)

            driver.close()
            this_area_name_list = json.dumps(this_area_name_list)

            doc = {'area_name' : name, "url" : url, "area_road_name" : this_area_name_list}
            interview_db = client.interview_test
            new_taipei_all_road_name = interview_db.new_taipei_all_road_name
            db_insert_one(new_taipei_all_road_name, doc)
            time.sleep(1)


def find_city_all(client):
    interview_db = client.interview_test
    data = interview_db.new_taipei_all_road_name

    return data.find()


def save_all_license(client):
    new_taipei_all = find_city_all(client)
    for data in new_taipei_all:
        for test in json.loads(data['area_road_name']):
            ua = UserAgent()
            U_Agent = ua.random
            OPTIONS = Options()
            OPTIONS.add_argument('--no-sandbox')
            OPTIONS.add_argument('--disable-dev-shm-usage')
            OPTIONS.add_argument('--disable-gpu')
            # OPTIONS.add_argument('--headless')
            extset = ['enable-automation', 'ignore-certificate-errors']
            OPTIONS.add_experimental_option("excludeSwitches", extset)  # 新版-關閉自動測試軟件
            OPTIONS.add_argument("user-agent=" + U_Agent)

            url = "https://building-management.publicwork.ntpc.gov.tw/bm_query.jsp?"
            driver = webdriver.Chrome(options=OPTIONS)
            driver.get(url)
            wait = WebDriverWait(driver, timeout=180)
            actions = ActionChains(driver)
            wait.until(expected.visibility_of_element_located((By.ID, "codeimg")))  # 等待IP
            img_x = driver.find_element("xpath", '//*[@id="codeimg"]')
            location = img_x.location
            size = img_x.size
            actions.move_to_element(img_x).perform()
            image_s = img_x.screenshot('codeimg.png')

            img_text_file = Image.open('codeimg.png')
            img_text = pytesseract.image_to_string(img_text_file)
            codeimg_input = driver.find_element("xpath", '//*[@id="Z1"]').send_keys(img_text)

            D3_input = driver.find_element("xpath", '//*[@id="D3"]').send_keys(test)
            D1V_input = driver.execute_script("document.getElementById('D1V').value='" + data['area_name'] + "'")
            A2V_input = driver.execute_script("document.getElementById('A2V').value='使用執照'")

            time.sleep(3)
            bouton_c = driver.find_element("xpath", '//*[@id="form_bm"]/div[2]/table/tbody/tr/td[2]/button').click()

            sys.exit('stop')


if __name__ == '__main__':
    user = "*****"
    password = "*****"
    connection_string = f"mongodb+srv://{user}:{password}@cluster0.loqpifh.mongodb.net/test"
    client = MongoClient(connection_string)

    # crawl_and_save_new_taipei_road_name(client) #抓取頁的新北地名清單入庫 至 Mongodb

    save_all_license(client)




